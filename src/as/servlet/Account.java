package as.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import as.servlet.bean.User;
import as.servlet.util.JDBCUtil;

/**
 * Servlet implementation class Account
 */
@WebServlet("/Account")
public class Account extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private DataSource ds;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public Account() {
    super();
  }

  /**
   * @see Servlet#init(ServletConfig)
   */
  public void init(ServletConfig config) throws ServletException {
    try {

      // Getting the datasource object used for database connectivity
      InitialContext ctx = new InitialContext();
      Context env = (Context) ctx.lookup("java:comp/env");
      ds = (DataSource) env.lookup("jdbc/simple_account");
    } catch (NamingException e) {
      e.printStackTrace();
    }
  }

  /**
   * @see Servlet#destroy()
   */
  public void destroy() {

  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException { // TODO URL rewriting

    String action = request.getParameter("action");

    if (action == null) {
      request.getRequestDispatcher("/index.jsp").forward(request, response);
    } else if (action.equals("login")) {
      request.getRequestDispatcher("/login.jsp").forward(request, response);
    } else if (action.equals("register")) {
      registerAction(request, response);
    } else {
      request.getRequestDispatcher("/index.jsp").forward(request, response);
    }
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
   *      response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    String formAction = request.getParameter("formAction");
    HttpSession session = request.getSession();

    if (formAction == null) {
      response.sendRedirect("/index.jsp");
    } else if (formAction.equals("login")) {
      
      if (logUserIn(request, response)) {
        request.getRequestDispatcher("/index.jsp").forward(request, response);
      } else {
        
        String email = request.getParameter("email");
        showLoginUnsuccessful(request, response, email);
      }
    } else if (formAction.equals("register")) {
      // registration form is not validated
      if (registerInDatabase(request, response)) {
        request.getRequestDispatcher("/index.jsp").forward(request, response);
      } else {
        // Takes user to generic error page
        throw new ServletException("Error while saving user data");
      }
    }
  }

  /**
   * Performs the register action - get request with action parameter=register.
   * Function checks if there exists a session user, if not, redirects user to login
   * page. If user exists in session we go to database to retrieve its data and
   * forward to register page
   * 
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void registerAction(HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException {

    User user = getUserFromSession(request);
    if (user == null) {
      request.getRequestDispatcher("/login.jsp").forward(request, response);
    } else {

      try (Connection con = ds.getConnection()) {

        user = JDBCUtil.getFullUserData(user, con);
        saveUserInSession(request, user);
        request.getRequestDispatcher("/register.jsp").forward(request, response);

      } catch (SQLException e) {
        throw new ServletException(e.getMessage()); // redirects to error page
      } catch (IllegalArgumentException e) {
        e.printStackTrace();
        request.getRequestDispatcher("/login.jsp").forward(request, response);
      }
    }
  }

  private boolean logUserIn(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    boolean result = true;
    String email = request.getParameter("email");
    String password = request.getParameter("password");

    try (Connection con = ds.getConnection()) {
      User user = new User(email, password);
      if (JDBCUtil.userExists(user, con)) {
        saveUserInSession(request, user);
      } else {
        result = false;
      }
    } catch (SQLException e) {
      result = false;
      throw new ServletException(e.getMessage());
    } catch (IllegalArgumentException e) {
      result = false;
    }
    return result;
  }
  
  /**
   * Saves User retrieved from request in database
   * @param request
   * @param response
   * @return
   */
  private boolean registerInDatabase(HttpServletRequest request,
      HttpServletResponse response) {
    boolean result = true;
    User user = getUserFromSession(request);
    user.setName(request.getParameter("name"));
    user.setSurname(request.getParameter("surname"));
    user.setPostcode(request.getParameter("postcode"));
    user.setStreet(request.getParameter("street"));

    try (Connection con = ds.getConnection()) {
      JDBCUtil.saveFullUserData(user, ds.getConnection());
      saveUserInSession(request, user);
    } catch (SQLException e) {
      e.printStackTrace();
      result = false;
    }

    return result;
  }

  /**
   * Fetches user object from session
   * 
   * @param request
   * @return User object retrieved from session
   */
  private User getUserFromSession(HttpServletRequest request) {
    HttpSession session = request.getSession();
    return (User) session.getAttribute("user");
  }

  /**
   * Saves user object in session
   * 
   * @param request
   * @param user
   *          - User object to be saved in session
   */
  private void saveUserInSession(HttpServletRequest request, User user) {
    HttpSession session = request.getSession();
    session.setAttribute("user", user);
  }

  /**
   * Forwards user to login login page with invalid login or password message and
   * saved email parameter to remember it in the form.
   * 
   * @param request
   * @param response
   * @param email
   * @throws ServletException
   * @throws IOException
   */
  private void showLoginUnsuccessful(HttpServletRequest request,
      HttpServletResponse response, String email)
      throws ServletException, IOException {
    request.setAttribute("message", "Invalid email or password");
    request.setAttribute("email", email);
    request.getRequestDispatcher("/login.jsp").forward(request, response);
  }

}
