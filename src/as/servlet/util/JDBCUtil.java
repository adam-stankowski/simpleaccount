package as.servlet.util;

import java.security.InvalidParameterException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import as.servlet.bean.User;

public class JDBCUtil {

  /**
   * Checks if a desired User exists in a database. Fires a request to database for a
   * given username and password and examines returned result set. If the result set
   * is empty - there are no rows method returns false;
   * 
   * @param user
   *          User to be checked for existence in a database
   * @param con
   *          Database connection
   * @return True is user with given username and password exists in a database,
   *         otherwise false
   * @throws SQLException
   *           if no such user is found
   */
  public static boolean userExists(User user, Connection con)
      throws SQLException, InvalidParameterException {
    boolean result = false;
    String sql = "SELECT COUNT(*) FROM users WHERE email=? and password=?";
    try (PreparedStatement ps = psForUser(sql, user, con,
        ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)) {

      ResultSet rs = ps.executeQuery();
      if (rs.first()) {
        result = rs.getInt(1) > 0;
      } else {
        throw new InvalidParameterException("No such user found");
      }
    }
    return result;
  }

  /**
   * Retrieves all columns for particular user and returns the newly updated user
   * with complete information from database
   * 
   * @param user
   *          - The user we're retrieving data for
   * @param con
   *          - connection object
   * @return - User with complete information from DB
   * @throws SQLException
   * @throws InvalidParameterException
   *           if no user is found
   */
  public static User getFullUserData(User user, Connection con)
      throws SQLException, InvalidParameterException {
    String sql = "SELECT name,surname,street,postcode FROM users WHERE email=? and password=?";
    try (PreparedStatement ps = psForUser(sql, user, con,
        ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)) {

      ResultSet rs = ps.executeQuery();
      if (rs.first()) {
        user.setName(rs.getString(1));
        user.setSurname(rs.getString(2));
        user.setStreet(rs.getString(3));
        user.setPostcode(rs.getString(4));
      } else {
        throw new InvalidParameterException("No such user found");
      }
    }

    return user;
  }

  /**
   * Saves all the information passed in User parameter to database.
   * 
   * @param user
   *          User object to save
   * @param con
   *          Database connection
   * @throws SQLException
   * @throws InvalidParameterException
   *           if no such user exists in database
   */
  public static void saveFullUserData(User user, Connection con)
      throws SQLException, InvalidParameterException {
    // PRIMARY KEY field needs to be selected for updates on ResultSet
    // email column is UNIQUE in database so we're sure, selecting it
    // returns single row.

    String sql = "SELECT id,name,surname,street,postcode FROM users WHERE email=? and password=?";
    try (PreparedStatement ps = psForUser(sql, user, con,
        ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE)) {

      ResultSet rs = ps.executeQuery();
      if (rs.next()) {
        rs.updateString(2, user.getName());
        rs.updateString(3, user.getSurname());
        rs.updateString(4, user.getStreet());
        rs.updateString(5, user.getPostcode());
        rs.updateRow();
      } else {
        throw new InvalidParameterException("No such user found");
      }
    }

  }

  /**
   * Helper function which returns PreparedStatement object for particular user
   * @param sql - SQL query we want to fire against the database
   * @param user - User object for which this query is fired
   * @param con - Connection to database
   * @param resultSetType - ResultSetType - Scrollable or not ? 
   * @param resultSetConcur - identifies if this result set is updatable
   * @return
   * @throws SQLException
   */
  private static PreparedStatement psForUser(String sql, User user, Connection con,
      int resultSetType, int resultSetConcur) throws SQLException {
    PreparedStatement ps = con.prepareStatement(sql, resultSetType, resultSetConcur);
    ps.setString(1, user.getEmail());
    ps.setString(2, user.getPassword());
    return ps;
  }

}
