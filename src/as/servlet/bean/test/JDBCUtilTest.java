package as.servlet.bean.test;

import static org.junit.Assert.*;

import java.security.InvalidParameterException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import as.servlet.bean.User;
import as.servlet.util.JDBCUtil;

/**
 * Unit tests the JDBCUtil class. This class uses a test database which is a copy of
 * the "production" database so that no data gets harmed. The connection object is
 * retrieved in setupBeforeClass using the standard DriverManager as there's no need
 * for a DataSource here.
 * 
 * @author Adam Stankowski
 *
 */
public class JDBCUtilTest {
  private static Connection con;

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    con = DriverManager.getConnection(
        "jdbc:mysql://localhost:3306/simple_account_test", "s_account_test",
        "simpleAccount");
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    if (con != null) {
      con.close();
    }
  }

  @Test
  public void testUserExists() throws SQLException {
    User user = new User("adam@wp.pl", "password");
    assertTrue(JDBCUtil.userExists(user, con));
  }

  @Test
  public void testUserNonExistent() throws SQLException {
    User user = new User("ad@wp.pl", "password");
    assertFalse(JDBCUtil.userExists(user, con));
  }

  @Test
  public void testUserWrongPassword() throws SQLException {
    User user = new User("adam@wp.pl", "wrongPassword");
    assertFalse(JDBCUtil.userExists(user, con));
  }

  @Test
  public void testGetFullUserData() throws SQLException {
    User user = new User("adam@wp.pl", "password");
    user = JDBCUtil.getFullUserData(user, con);
    assertEquals("Adam", user.getName());
    assertEquals("TestAdam", user.getSurname());
    assertEquals("TestStreet", user.getStreet());
    assertEquals("TestPostco", user.getPostcode());
  }

  @Test(expected = InvalidParameterException.class)
  public void testGetFullUserDataNonExistentUser() throws SQLException {
    User user = new User("nonexistent@wp.pl", "password");
    user = JDBCUtil.getFullUserData(user, con);
  }

  @Test
  public void testSaveAndGetFullUserData() throws SQLException {
    User user = new User("adam@wp.pl", "password");
    user.setName("UpdatedName");
    user.setSurname("UpdatedSurname");
    user.setStreet("UpdatedStreet");
    user.setPostcode("UpdatedPos");
    JDBCUtil.saveFullUserData(user, con);

    User retrieved = new User("adam@wp.pl", "password");
    retrieved = JDBCUtil.getFullUserData(retrieved, con);
    assertEquals("UpdatedName", retrieved.getName());
    assertEquals("UpdatedSurname", retrieved.getSurname());
    assertEquals("UpdatedStreet", retrieved.getStreet());
    assertEquals("UpdatedPos", retrieved.getPostcode());

    // Going back to previous state
    user.setName("Adam");
    user.setSurname("TestAdam");
    user.setStreet("TestStreet");
    user.setPostcode("TestPostco");
    JDBCUtil.saveFullUserData(user, con);

  }

  @Test(expected = InvalidParameterException.class)
  public void testSaveFullUserDataNonExistentUser() throws SQLException {
    User user = new User("nonexistent@wp.pl", "password");
    JDBCUtil.saveFullUserData(user, con);
  }

}
