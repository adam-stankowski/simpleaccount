package as.servlet.bean.test;

import static org.junit.Assert.*;

import org.junit.Test;

import as.servlet.bean.User;

public class UserTest {

  @Test
  public void validateCorrectEmailTest() {
    String email = "adam@wp.pl";
    User u = new User(email, "password");
  }

  @Test(expected = IllegalArgumentException.class)
  public void validateEmailWithoutDotTest() {
    String email = "adam@wp";
    User u = new User(email, "password");
  }

  @Test(expected = IllegalArgumentException.class)
  public void validateEmailWithoutPLTest() {
    String email = "adam@wp.";
    User u = new User(email, "password");
  }

  @Test(expected = IllegalArgumentException.class)
  public void validateEmailWithoutPLAndFirstWordTest() {
    String email = "@wp.";
    User u = new User(email, "password");
  }

  @Test(expected = IllegalArgumentException.class)
  public void validateEmailWithoutWordAfterAtTest() {
    String email = "adam@.pl";
    User u = new User(email, "password");
  }

  @Test(expected = IllegalArgumentException.class)
  public void validateEmptyEmailTest() {
    String email = "";
    User u = new User(email, "password");
  }

  @Test(expected = IllegalArgumentException.class)
  public void validateSingleNameEmailTest() {
    String email = "adam";
    User u = new User(email, "password");
  }

  @Test(expected = IllegalArgumentException.class)
  public void validateSingleNameDotPlEmailTest() {
    String email = "adam.pl";
    User u = new User(email, "password");
  }

  @Test(expected = IllegalArgumentException.class)
  public void validatePLOnlyEmailTest() {
    String email = ".pl";
    User u = new User(email, "password");
  }

  @Test(expected = IllegalArgumentException.class)
  public void validateAtOnlyEmailTest() {
    String email = "@";
    User u = new User(email, "password");
  }

  @Test(expected = IllegalArgumentException.class)
  public void validateAtPLOnlyEmailTest() {
    String email = "@.pl";
    User u = new User(email, "password");
  }

  @Test(expected = IllegalArgumentException.class)
  public void validateEmailWithSpaceTest() {
    String email = "ada m@wp.pl";
    User u = new User(email, "password");
  }

  @Test
  public void validateCorrectPasswordTest() {
    String password = "atLeast6Chars";
    User u = new User("adam@wp.pl", password);
  }

  @Test
  public void validateCorrectPasswordNumbersOnlyTest() {
    String password = "123456";
    User u = new User("adam@wp.pl", password);
  }

  @Test(expected = IllegalArgumentException.class)
  public void validatePasswordTooShortTest() {
    String password = "short";
    User u = new User("adam@wp.pl", password);
  }

  @Test(expected = IllegalArgumentException.class)
  public void validateEmptyPasswordTest() {
    String password = "";
    User u = new User("adam@wp.pl", password);
  }

  @Test(expected = IllegalArgumentException.class)
  public void validateWithSpacePasswordTest() {
    String password = "AtLeast6Char s";
    User u = new User("adam@wp.pl", password);
  }

}
