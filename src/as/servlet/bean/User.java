package as.servlet.bean;

/**
 * A simple, bean style class repsesenting user in the system It constructs a user
 * and checks for correctness of email and password. If not correct, it throws an
 * IllegalArgumentException out of its constructor. Client program needs to handle
 * this exception.
 * 
 * @author Adam Stankowski
 *
 */
public class User {
  private String email, name, surname, password, street, postcode;

  public User(String email, String password) throws IllegalArgumentException {
    if (!validate(email, password)) {
      throw new IllegalArgumentException();
    }

    this.email = email;
    this.password = password;
    this.name = "";
    this.surname = "";
    this.street = "";
    this.postcode = "";
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  /**
   * Function called to validate email correctness based on simple rules - exercise
   * only. The rules are following: a non space word before and after @, a dot and a
   * non space word.
   * 
   * @param aEmail
   *          - an email to be verified
   * @return
   */
  private boolean validateEmail(String aEmail) {
    //
    String eMailRegex = "\\S\\w+[@]\\S\\w+\\.\\S\\w+";
    return aEmail.matches(eMailRegex);
  }

  /**
   * Validates correctness of password based on simple rules - exercise only. The
   * rules are following: a Word without a space at least 6 characters. small,
   * capital or numbers
   * 
   * @param aPassword
   *          - a password to be checked for correctness
   * @return
   */
  private boolean validatePassword(String aPassword) {
    // a Word without a space at least 6 times. I'm not sure why
    // I had to put 5 here instead of 6. Tests failed otherwise
    String passwordRegex = "\\S\\w{5,}";
    return aPassword.matches(passwordRegex);
  }

  /**
   * Aggregates the correctness of email and password
   * 
   * @param aEmail
   *          - an email to be checked for correctness
   * @param aPassword
   *          - a password to be checked for correctness
   * @return
   */
  private boolean validate(String aEmail, String aPassword) {
    return validateEmail(aEmail) && validatePassword(aPassword);
  }

}
