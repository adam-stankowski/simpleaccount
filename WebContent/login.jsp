<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="styles.css"/>
<title>Simple Registration</title>

</head>
<body>
<% String message = (String)request.getAttribute("message");
String email = (String)request.getAttribute("email");
%>

<div id="content">
    <form id="loginForm" action="<%= response.encodeURL(request.getContextPath()+ "/Account") %>" method="post">
        <input type="hidden" name="formAction" value="login"/>
        <div class="formRow">
            <label for="email">Email</label>
            <input id="email" type="text" name="email" value="<%= email == null ? "" : email %>"/>
        </div>
        <div class="formRow">
            <label for="password">Password</label>
            <input id="password" type="password" name="password" value=""/>
        </div>
        <input class="submit" type="submit" value = "login"/>    
    </form>
    <span class="errorMessage">
    <%= message == null ? "" : message %>    
    </span>
</div>

</body>
</html>