<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isErrorPage="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Error</title>
</head>
<body>
<div id="content">
	<p>
	   Oops ! Something didn't quite go well <br/>
	   <%= exception.getMessage() %> <br/>
	   	
	</p>
	<p>
	<a href="<%= response.encodeURL(request.getContextPath()+ "/Account") %>">Back to homepage</a>
	</p>
	
</div>
</body>
</html>