<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Simple registration</title>
</head>
<body>
<%@ page import="as.servlet.bean.User" %>
<%
User user = (User)session.getAttribute("user");
if(user == null){
%>
<a href="<%= response.encodeURL(request.getContextPath()+ "/Account?action=login") %>">Login</a>
<%
}else{
%>
<span>Hello <%=user.getEmail() %></span>
<br/><br/>
<a href="<%= response.encodeURL(request.getContextPath()+ "/Account?action=register") %>">Complete Profile</a>
<% } %>
</body>
</html>