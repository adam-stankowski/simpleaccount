<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="styles.css"/>
<title>Register</title>

</head>
<body>
<%@ page import="as.servlet.bean.User" %>
<%
User user = (User)session.getAttribute("user");

// When a user's not logged in and accesses a page via URL, 
// SimpleAccount/Account?action=register
// transfer him to login page
if(user == null){
  request.getRequestDispatcher("/login.jsp").forward(request, response);
}
%>
<div id="content">
<form id="registerForm" action="<%= response.encodeURL(request.getContextPath()+ "/Account") %>" method="post">
    <input type="hidden" name="formAction" value="register"/>
    <div class="formRow">
        <label for="email">Email</label>
        <input id="email" type="text" name="email" value="<%= user==null ? "" : user.getEmail() %>" disabled="disabled"/>
    </div>
    
    <div class="formRow">
        <label for="name">Name</label>
        <input id="name" type="text" name="name" value="<%= user==null ? "" : user.getName() %>"/>
    </div>
    
    <div class="formRow">
        <label for="surname">Surname</label>
        <input id="surname" type="text" name="surname" value="<%= user==null ? "" : user.getSurname() %>"/>
    </div>
    
    <div class="formRow">
        <label for="street">Street</label>
        <input id="street" type="text" name="street" value="<%= user==null ? "" : user.getStreet() %>"/>
    </div>
    
    <div class="formRow">
        <label for="postcode">Postcode</label>
        <input id="street" type="text" name="postcode" value="<%= user==null ? "" : user.getPostcode() %>"/>
    </div>
    
    <div class="formRow">
        <input type="submit" value="Submit"/>
    </div>
        
</form>

</div>

</body>
</html>