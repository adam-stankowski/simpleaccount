# README #

This is a simple application, using a basic JSP, for user login and filling user details. It's probably not an industry standard of handling user authorization in Java but it illustrates few simple mechanisms which can be a base for future development. 

## Important mechanisms included in this app are ##
* MVC structure
* Handling form data in Servlet
* Storing user data in session
* URL rewriting 
* Connecting to database with JNDI DataSource object
* Generic error page

### What is this repository for? ###

This code comes with no guarantee whatsoever and should not be used for commercial purposes. It's here only for learning purposes, feel free to use the code snippets included here to develop your own applications, and if you find any better way of doing things, it would be perfect if you could let me know. 

### How do I get set up? ###

In order to set this project up, the only thing required should be to fork the repo and make sure you have a MySQL connector .jar file included in the build path as well as your Tomcat's lib folder. 
SQL scripts to create a database, tables and example data are included. 

### Testing ###

The .test package contains unit tests that I used to verify required java class functionality. For JDBCUtil testing I used a separate test database which was a copy of the original database, so that "production" data is not harmed. The connection to test database was retrieved in setUpBeforeClass() function and used for all the unit tests. 
I found this way of handling database tests quite convenient, but if you have any other approach to this, it would be great if you could let me know, always happy to learn :) 

### Who do I talk to? ###

* Repo owner or admin